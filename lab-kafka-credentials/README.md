# Kafka lab cluster

Gaia-X Lab provides you a free of charge kafka cluster that you can use to send your compliance VerifiableCredentials

Cluster : kafka-8cb9a509-o53bc81e4.database.cloud.ovh.net:20186
Use SSL auth with the ca, user-certificate and user-key provided in this folder to connect

TopicName: gaiaxComplianceCredential
You can put whatever you want as a clientId. Please note that your offset on the kafka topic is linbked to your groupId.


For example in typescript


```typescript
        const {SSL_CA, SSL_CERTIFICATE, SSL_KEY} = this.loadFromEnvOrDisk();
        const kafkaConfig: KafkaConfig = {
            brokers: process.env.KAFKA_BROKERS!.split(","),
            clientId: "yourClientId",
            ssl: {
                ca: SSL_CA,
                cert: SSL_CERTIFICATE,
                key: SSL_KEY,
            },
        };
        this.kafka = new Kafka(kafkaConfig);
        this.VPProducer = this.kafka.producer();

```