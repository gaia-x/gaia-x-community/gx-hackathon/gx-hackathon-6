package com.pjc.gaiax.Api.Stages;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class DIDLint extends Stage {
    
    public String lintConnector (String did_str){

    final String LINT_URL = "http://localhost:3000/api/validate/";

    URL lint_url;
    HttpURLConnection httpConn;
    try {
        lint_url = new URL(LINT_URL.concat(did_str));
        httpConn = (HttpURLConnection) lint_url.openConnection();
        httpConn.setRequestMethod("GET");
        int responseCode = httpConn.getResponseCode();
        
        if(responseCode == HttpURLConnection.HTTP_OK){
            BufferedReader in = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
            String inputLine;
            StringBuffer jsonResponse = new StringBuffer();

            while((inputLine = in.readLine()) != null){
                jsonResponse.append(inputLine);
            }
            in.close();
            return jsonResponse.toString();
        }
    } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
    return "{'valid':false}";
    
    }

}
