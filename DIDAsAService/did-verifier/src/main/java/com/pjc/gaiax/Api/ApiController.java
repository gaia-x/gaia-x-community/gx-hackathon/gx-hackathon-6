package com.pjc.gaiax.Api;

import org.apache.catalina.connector.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import jakarta.annotation.PostConstruct;
import jakarta.websocket.server.PathParam;


import java.security.SecureRandom;
import java.util.Map;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.MACSigner;
import com.pjc.gaiax.Api.Stages.DIDLint;

@RestController
public class ApiController {

   
    



   @GetMapping(value = "/api/init")
   public ResponseEntity<String> getFirstCalll(@PathParam("jose") String var_first) {

    JWSObject jwsObject = new JWSObject(new JWSHeader(JWSAlgorithm.HS256),
    new Payload("Hello, world!"));

    // We need a 256-bit key for HS256 which must be pre-shared
    byte[] sharedKey = new byte[32];
    new SecureRandom().nextBytes(sharedKey);

    // Apply the HMAC to the JWS object
    try {
    jwsObject.sign(new MACSigner(sharedKey));


    // Output in URL-safe format
    System.out.println(jwsObject.serialize());
    
    } catch (JOSEException e) {
    // TODO Auto-generated catch block
    e.printStackTrace();
}
    return new ResponseEntity<String>("ok", HttpStatus.OK);
 }

 @GetMapping(value = "/checkDIDStatus/{id}")
 public ResponseEntity<String> checkDIDStatus(@PathVariable("id") String id) {
    DIDLint lintConnector = new DIDLint();

  return new ResponseEntity<String>(lintConnector.lintConnector(id), HttpStatus.OK);
}
@PostMapping(value = "/checkDID ")
public ResponseEntity<List<ValidationResult>> checkDID(@PathVariable("id") String id), @PathParam("context") String var_second) {
  
    
    
 
 
 return new ResponseEntity<String>("ok", HttpStatus.OK);
}

}
//o ter uma API com o seguinte: /checkDIDStatus/{id} (metod get) e /checkDID (method post) que recebe 2 parâmetros (um json e uma string)