# Secure DID Document Checker
Verify DID Document integrity, cypher and pubkey correctness agains policy, privacy on URIs against allowlists, rules against specific DataSpaces constraints.


## checkDIDLint(DID-Doc)                        
Check DID document integrity against DID LINT 
using the DID-LINT service
 install [DID-LINT docker](https://github.com/OwnYourData/didlint)
Call it with @paulocaretta  [did-checker](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/tree/deal-ex-machina-exaion/DIDAsAService)


## checkDIDKeys(DiD-Doc, Keypolicies)           
Check Key policies – ex RSA >2048, PubKey correctness, Type of cypher
using simple rule format , services like rego later.
check [JWT2020](https://w3c-ccg.github.io/lds-jws2020/#introduction)
based on [iana.org/JOSE](https://www.iana.org/assignments/jose/jose.xhtml)
        
        check 
            type : "JsonWebKey2020"
            kty : "OKP"
            crb : "Ed25519"
            x : 
            y :
            e :
            n :

## checkDIDPrivacy (DID-Doc, PrivacyPolicies)   
Check privacy – against a defined allowlist, URI structure (and check x509 etc. ) 
Objective is to detect rogue or unallowed URIs, or enforce use of specific URIs, or CID IPFS etc.
using simple rule file , services like rego later.

        check 
            all URI 
            against 
                allowlist (URI)
                exclusion list (URI)
            // should we check x509 certificate allowed autrority ??? 
            all endpoints 
                ping curl , return 200 , protected, not  // There could be also service avaialabilty checks.  

## checkDIDDSPolicy (DID-Doc, DS-policy)                           
Check specific data space policies / constraints on DID-DOC
using simple rule file , services like rego later.

        ruleset {
            name : "Catena-X"
            version : "0.1"
            signature : ""
            validityDate : ""
                type : keyConstraints 
                        kty == "EC"
                        crv == "256"
                type : privacyConstraints
                        uri.allow (gaia-x, catena-x etc.) 
                        uri.exclude (ur1, ur2)
                type : specificProperties
                        field "foo" <> null
                        field "bar" exist
                        field "foo2" in (a,b,c)

        }

Evaluate the opportunity to have a single json with mandatory checks , standard policies, and then "custom fields" -- data spaces can be nested so the logic of policies must be checked and constistent, and signed by each authority. To be discussed.