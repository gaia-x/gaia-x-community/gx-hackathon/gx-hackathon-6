# [Implementing Verifiable Credentials in a real live use case](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Proposals#17-implementing-verifiable-credentials-in-a-real-live-use-case)

## Participants

1. https://www.smartsensesolutions.com/
2. https://www.vereign.com/
3. https://www.aws-institut.de/
4. https://www.trueocean.io/
5. https://startinblox.com/

## Repository Details

1. smartsense-gaia-x-api: Spring boot application exposing APIs to frontend 
2. smartsense-gaia-x-signer: NodeJS generic tools to create VC/VP, did:wed and onboarding into Gaia-x
3. smartsense-gaia-x-ui: Angular UI application


This is POC to showcase the capability of smartSense relevant to Gaia-X ecosystem.

This POC covers below user case:

1. On-boarding in Gaia-x
    1. Create a sub-domain for enterprise
    2. Create SSL key-value pair for enterprise
    3. Create web did
    4. Create participant credentials and sign in using Gaia-x API
    5. Host public key, did.json, and participant files under the .well-known path
2. Create service offering and create service offering credential and host offer file under the .well-known path
3. List Catalogue
4. Offer Credential using OCM
5. Store Credential in PCM
6. Create VP using OCM
7. Share credential from PCM
8. Login with PCM

## Tools and Technologies

1. Spring boot with JPA
2. K8S Java SDK
3. Certbot SDK acme4j
4. AWS Route53 SDK
5. AWS S3 SDK
6. [NodeJS for signer tool](https://github.com/smartSenseSolutions/smartsense-gaia-x-signer)
7. [GXFS PCM](https://apps.apple.com/in/app/gxfs-pcm/id1662845551)
8. [GXFS Connection manager](https://gitlab.com/gaia-x/data-infrastructure-federation-services/ocm/connection-manager)
9. [GXFS proof manager](https://gitlab.com/gaia-x/data-infrastructure-federation-services/ocm/proof-manager/-/blob/main/swagger.json)
10. [Tiny URL](https://tinyurl.com/)

## Onboarding flow

![onboarding.png](smartsense-gaia-x-api%2Fdoc%2Fonboarding.png)

## Create service offer flow

![Create service offer.png](smartsense-gaia-x-api%2Fdoc%2FCreate%20service%20offer.png)

## Consume service flow

![Service offer flow.png](smartsense-gaia-x-api%2Fdoc%2FService%20offer%20flow.png)

## High level could deployment diagram

![Gaia-x POC.drawio.png](smartsense-gaia-x-api%2Fdoc%2FGaia-x%20POC.drawio.png)

## Known issue or improvement

1. Authentication and Authorization flow can be improved(using OpenID4VP/SIOPV2)
2. Data exchange based on Gaia-x trust framework(Ocean protocol??)
3. Unit Test
4. K8S ingress and secret creation can be done using argoCD/argo workflow
5. Storage of key and other files can be done in better and more secure way

## Run application

To be added

### Configuration

1. Create k8s user with access to ingress and secret creation
2. Create AWS s3 bucket
3. Create hosted zone in AWS with your base domain
4. Create an AWS IAM user with the access to hosted zone and S3

### Run in IntelliJ Idea

1. Set values in the application.yaml
2. Run using the Intellij idea

### Run in k8s

Please refer to sample config files in ``/k8s`` folder

## References

1. [Create SSL certificate using acme4j](https://github.com/shred/acme4j/blob/master/acme4j-example/src/main/java/org/shredzone/acme4j/example/ClientTest.java)

### **Note: The demo apps are not meant to be production-ready nor starter-kits but just a way to show this module components and their usage.**
